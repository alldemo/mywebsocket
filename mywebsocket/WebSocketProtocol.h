//
// WebSocketProtocol.h
//  websocketserver
//
//  Created by cl.zhan on 17/5/25.
//  email:zhanchonglongba@126.com/690759587
//
//

#ifndef WEBSOCKETPROTOCOL_H_H_
#define  WEBSOCKETPROTOCOL_H_H_

#include <stdint.h>

class WebSocketProtocol
{
public:
	WebSocketProtocol();
	void encode(char *data, unsigned int &len);
	void decode(const char *msg, unsigned int len);
	const char *payload_data();
	uint64_t payload_length() const;
	uint8_t opcode() const;

private:
	void init();

private:
	enum {
		MAXLINE = 2048
	};
	uint8_t _fin;
	uint8_t _opcode;
	uint8_t _mask;
	uint8_t _masking_key[4];
	uint64_t _payload_length;
	char _payload_data[MAXLINE];
};



#endif

