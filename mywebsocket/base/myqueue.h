﻿/********************************************************************
创建时间：	
文件名：		
作者：		

功能：		1. 线程安全队列
            2.

说明：	    1. 多线程状态下的安全队列
            2.
*********************************************************************/
#ifndef MYQUEUE_H_
#define MYQUEUE_H_

#include <queue>//std queue
#include "Lock.h"
#include "Base.h"
#include "XLog.h"

namespace base{

	template<typename TYPE>
	class CMyQueue
	{
	private:
		std::queue<TYPE> theQueue_;
		base::Lock m_oMutex;
		DISALLOW_COPY_AND_ASSIGN(CMyQueue);

	public:
		CMyQueue(){};
		~CMyQueue()
		{
			//互斥锁
			AutoLock alock(m_oMutex);
			while (!theQueue_.empty())
			{
				TYPE tmp = theQueue_.front();
				if (tmp.data)
				{
					delete tmp.data;
				}
				theQueue_.pop();
			}
		};
	public:
		TYPE pop();//出一个队列
		void push(const TYPE &val);//压入一个队列
		size_t size();//队列长度
		bool empty();//判空
	};
	template<typename TYPE>
	TYPE CMyQueue<TYPE>::pop()
	{
		AutoLock alock(m_oMutex);//互斥锁
		xbase::XLog::Printf("CMyQueue", xbase::XLOG_INFO, "current SendTransport queue size = %d", theQueue_.size());
		if (!theQueue_.empty())
		{
			TYPE tmp = theQueue_.front();
			theQueue_.pop();
			return tmp;
		}
		else
			return TYPE();//空的返回默认
	}

	template<typename TYPE>
	void CMyQueue<TYPE>::push(const TYPE &val)
	{
		AutoLock alock(m_oMutex);//互斥锁
		theQueue_.push(val);
	}

	template<typename TYPE>
	size_t CMyQueue<TYPE>::size()
	{
		//AutoLock alock(m_oMutex);//互斥锁
		//	size_t size=theQueue_.size();
		return theQueue_.size();
	}

	template<typename TYPE>
	bool CMyQueue<TYPE>::empty()
	{
		AutoLock alock(m_oMutex);//互斥锁
		return theQueue_.empty();
	}
}


#endif