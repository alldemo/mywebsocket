﻿
#include "Lock.h"


namespace base {

Lock::Lock()
{
#if defined(WIN32)
    // 2000 指的是在等待进入临界区时自旋个数，防止线程睡眠
    ::InitializeCriticalSectionAndSpinCount(&cs_, 2000);
#else
	pthread_mutex_init(&mutex,NULL);
#endif
}

Lock::~Lock()
{
#if defined(WIN32)
    ::DeleteCriticalSection(&cs_);
#else
	pthread_mutex_destroy(&mutex);
#endif
}

bool Lock::Try()
{
#if defined(WIN32)
    if (::TryEnterCriticalSection(&cs_) != FALSE) {
        return true;
    }
    return false;
#else
	pthread_mutex_trylock(&mutex);
#endif
}

void Lock::Acquire()
{
#ifdef WIN32
	::EnterCriticalSection(&cs_);
#else
	pthread_mutex_lock(&mutex);
#endif


}

void Lock::Release()
{
#ifdef WIN32
	::LeaveCriticalSection(&cs_);
#else
	pthread_mutex_unlock(&mutex);
#endif
}

}
