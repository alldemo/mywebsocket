#ifndef H264_TOOLS_H_
#define H264_TOOLS_H_

#include "Base.h"

#define NALU_TYPE_SLICE     1
#define NALU_TYPE_DPA       2
#define NALU_TYPE_DPB       3
#define NALU_TYPE_DPC       4
#define NALU_TYPE_IDR       5
#define NALU_TYPE_SEI       6
#define NALU_TYPE_SPS       7
#define NALU_TYPE_PPS       8
#define NALU_TYPE_AUD       9
#define NALU_TYPE_EOSEQ     10
#define NALU_TYPE_EOSTREAM  11
#define NALU_TYPE_FILL      12



static const uint8_t *ff_avc_find_startcode_internal(const uint8_t *p, const uint8_t *end)
{
	const uint8_t *a = p + 4 - ((intptr_t)p & 3);

	for (end -= 3; p < a && p < end; p++) {
		if (p[0] == 0 && p[1] == 0 && p[2] == 1)
			return p;
	}

	for (end -= 3; p < end; p += 4) {
		uint32_t  x = *(const uint32_t *)p;
		//      if ((x - 0x01000100) & (~x) & 0x80008000) // little endian
		//      if ((x - 0x00010001) & (~x) & 0x00800080) // big endian
		if ((x - 0x01010101) & (~x) & 0x80808080) { // generic
			if (p[1] == 0) {
				if (p[0] == 0 && p[2] == 1)
					return p;
				if (p[2] == 0 && p[3] == 1)
					return p + 1;
			}
			if (p[3] == 0) {
				if (p[2] == 0 && p[4] == 1)
					return p + 2;
				if (p[4] == 0 && p[5] == 1)
					return p + 3;
			}
		}
	}

	for (end += 3; p < end; p++) {
		if (p[0] == 0 && p[1] == 0 && p[2] == 1)
			return p;
	}

	return end + 3;
}

static const uint8_t *ff_avc_find_startcode(const uint8_t *p, const uint8_t *end)
{
	const uint8_t *out = ff_avc_find_startcode_internal(p, end);
	if (p<out && out<end && !out[-1]) out--;
	return out;
}

static void ff_avc_parse_nal_units(const uint8_t *bufIn, int inSize, uint8_t* bufOut, int* outSize)
{
	const uint8_t *p = bufIn;
	const uint8_t *end = p + inSize;
	const uint8_t *nal_start, *nal_end;

	uint8_t* pbuf = bufOut;

	*outSize = 0;
	nal_start = ff_avc_find_startcode(p, end);
	while (nal_start < end)
	{
		while (!*(nal_start++));

		nal_end = ff_avc_find_startcode(nal_start, end);

		unsigned int nal_size = nal_end - nal_start;

		pbuf[0] = (nal_size >> 24) & 0xff;
		pbuf[1] = (nal_size >> 16) & 0xff;
		pbuf[2] = (nal_size >> 8) & 0xff;
		pbuf[3] = (nal_size)& 0xff;
		pbuf = pbuf + 4;

		memcpy(pbuf, nal_start, nal_size);
		pbuf += nal_size;

		nal_start = nal_end;
	}

	*outSize = (pbuf - bufOut);
}


#endif //H264_TOOLS_H_

/**
参考文档：
ADTS格式：(AAC的两种格式之一，用于音频流)
ADTS vs. ADIF http://blog.csdn.net/wlsfling/article/details/5876016
ADTS帧头部格式： http://wiki.multimedia.cx/index.php?title=ADTS

=============================================================================
.aac文件从头到尾已经是ADTS帧构成的了，我们的测试文件quicksort.aac的ADTS帧为： FF F1 4C 40 0B BF FC (十六进制) ...

二进制比特对应关系：
AAAA AAAA AAAA BCCD EEFF FFGH HHIJ KLMM MMMM MMMM MMMO OOOO OOOO OOPP (QQQQQQQQ QQQQQQQQ)
1111 1111 1111 0001 0100 1100 0100 0000 0000 1011 1011 1111 1111 1100

二进制：
A = 1111 1111 1111	==> AAC ADTS帧头同步比特
B = 0	==> 0: MPEG-4 / 1: MPEG-2
C = 00
D = 1	==> protection absent ==> 1: no CRC / 0: CRC
E = 01	==> the MPEG-4 Audio Object Type minus 1 ==> aacObjectType=2: AAC LC (Low Complexity) 参见：http://wiki.multimedia.cx/index.php?title=MPEG-4_Audio#Audio_Object_Types
F = 0011	==> MPEG-4 Sampling Frequency Index ==> 3: 48000 Hz / 4: 44100Hz	参见：http://wiki.multimedia.cx/index.php?title=MPEG-4_Audio#Sampling_Frequencies
G = 0
H = 001		==> MPEG-4 Channel Configuration  ==> 1: 1 channel: front-center / 2: 2 channels: front-left, front-right 参见：http://wiki.multimedia.cx/index.php?title=MPEG-4_Audio#Channel_Configurations
I = 0		==> 0: encoding / 1: decoding
J = 0		==> 0: encoding / 1: decoding
K = 0		==> 0: encoding
L = 0		==> 0: encoding
M = 00 0000 1011 101	==> frame length, this value must include 7 or 9 bytes of header length: FrameLength = (ProtectionAbsent == 1 ? 7 : 9) + size(AACFrame) ==> 93(dec) = 7 + size(AACFrame)
O = 1 1111 1111 11	==> buffer fullness
P = 00		==> number of AAC frames in ADTS frame minus 1 ==> 1 AAC frame
**/
