﻿
#ifndef _BASE_WAIT_EVENT_
#define _BASE_WAIT_EVENT_

#include "Base.h"

namespace base {

#if defined(WIN32)

#define event_handle HANDLE 

#else  
#include <pthread.h>  
	typedef struct
	{
		bool state;
		bool manual_reset;
		pthread_mutex_t mutex;
		pthread_cond_t cond;
	}event_t;
#define event_handle event_t*

#endif

class WaitableEvent
{
 public:
  // If manual_reset is true, then to set the event state to non-signaled, a
  // consumer must call the Reset method.  If this parameter is false, then the
  // system automatically resets the event state to non-signaled after a single
  // waiting thread has been released.
	 WaitableEvent(bool manual_reset, bool init_state);

  ~WaitableEvent();

  int Reset();

  int Signal();

  // Returns true if the event is in the signaled state, else false.  If this
  // is not a manual reset event, then this test will cause a reset.
  int IsSignaled();

  int Wait();

  int TimedWait(unsigned int millsecs);

  event_handle handle() const { return handle_; }

 
 private:
  friend class WaitableEventWatcher;

  event_handle  handle_;

  DISALLOW_COPY_AND_ASSIGN(WaitableEvent);
};

}  // namespace hbase

#endif  // _BASE_WAIT_EVENT_
