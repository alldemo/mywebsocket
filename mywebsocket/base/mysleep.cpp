
#include "msleep.h"

#include "Base.h"

#if defined(WIN32)
#include <windows.h> 
#else
#include <unistd.h>  
#include <time.h>  
#endif

void Msleep(int ms)
{
	//::usleep(ms * 1000);  
	// 好像使用nanosleep更好  
#if defined(WIN32)
	::Sleep(ms);
#else
	timespec ts;
	ts.tv_sec = ms / 1000;
	ts.tv_nsec = (ms % 1000) * 1000000;
	nanosleep(&ts, NULL);
#endif

}