
#include <stdio.h>
#include "CWebSocketServer.h"
#include "base/msleep.h"

int main(int argc, char * argv[])
{

	CWebSocketServer *server = new CWebSocketServer();

    server->Initialization(9000);

    char str[100] = {0};
    while(true)
    {
        if(fgets(str, 100, stdin) == NULL)
        {
            Msleep(500);
            continue;
        }
        printf(" str from user %s\n",str);
        if(strncmp(str, "quit", 4) == 0)
        {
            break;
        }

    }

    server->Stop();

    if(server)
    {
        delete server;
        server = NULL;
    }

    return 0;
}
