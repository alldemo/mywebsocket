CONFIG += c++11

TARGET = mywebsocket
CONFIG += console

TEMPLATE = app


unix{
DEFINES +=  LINUX

INCLUDEPATH +=$$PWD/  \
               


#LIBS += -L$$PWD/../linux_bin/sdk/ \
#        -Wl,-rpath=$$PWD/../linux_bin/sdk/ \
#         -lSRRtcEngine -lmediacore -lcallserver -lCryptoWrapper -lapm

}


win32{
DEFINES +=  WIN32

INCLUDEPATH +=$$PWD/  \


LIBS +=

}



SOURCES +=  \
    base/Lock.cpp \
    base/mysleep.cpp \
    base/SimpleThread.cpp \
    base/WaitEvent.cpp \
    base64.cpp \
    CWebSocketServer.cpp \
    server.cpp \
    sha1.cpp \
    WebsocketHandler.cpp \
    WebSocketProtocol.cpp \
    WebsocketRequest.cpp \
    WebsocketRespond.cpp
   


# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    base/Base.h \
    base/DataBuffer.h \
    base/h264tools.h \
    base/Lock.h \
    base/msleep.h \
    base/myqueue.h \
    base/SimpleThread.h \
    base/WaitEvent.h \
    base64.h \
    CWebSocketServer.h \
    sha1.h \
    WebsocketHandler.h \
    WebSocketProtocol.h \
    WebsocketRequest.h \
    WebsocketRespond.h
   

