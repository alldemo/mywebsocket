//
//  CWebSocketServer.h
//  websocketserver
//
//  Created by cl.zhan on 17/5/25.
//  email:zhanchonglongba@126.com/690759587
//
//

#ifndef CWEBSOCKETSERVER_H_H_
#define CWEBSOCKETSERVER_H_H_



#if defined(WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <Winsock2.h>
#include <WS2tcpip.h>
#include <ws2bth.h>
#endif

#if defined(LINUX)
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include "base/SimpleThread.h"
#include "WebsocketHandler.h"

#include <string>
#include <vector>

class CWebSocketServer :public base::SimpleThread
{
public:
	CWebSocketServer();
	~CWebSocketServer();


	int  Initialization(int port);
	int _deinitialize();

	int Stop();
	void Run();

private:
	int _socket;

    std::vector<WebsocketHandler*>  mWebSocketHandlerVector;

};


#endif
