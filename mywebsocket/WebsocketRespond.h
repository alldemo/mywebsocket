//
//  WebsocketRequest.h
//  websocketserver
//
//  Created by cl.zhan on 17/5/25.
//  email:zhanchonglongba@126.com/690759587
//
//

#ifndef WEBSOCKETRESPOND_H_H_
#define WEBSOCKETRESPOND_H_H_

#include <map>
#include <string>
#include <sstream>



class WebsocketRespond
{
public:
	WebsocketRespond();
	~WebsocketRespond();

	int handshark(int fd, const char *buf, unsigned int len);

private:
	int fetch_http_info(const char *buf, unsigned int len);

	int send_data(int fd, char *buff, int len);

private:
	
};


#endif