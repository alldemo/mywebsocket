//
//  WebsocketHandler.h
//  websocketserver
//
//  Created by cl.zhan on 17/5/25.
//  email:zhanchonglongba@126.com/690759587
//
//

#ifndef  WEBSOCKETHANDLER_H_H_
#define  WEBSOCKETHANDLER_H_H_

#define BUFFLEN 2048

#include <string.h>
#include "WebsocketRequest.h"
#include "WebsocketRespond.h"

enum WEBSOCKET_STATUS {
	WEBSOCKET_UNCONNECT = 0,
	WEBSOCKET_HANDSHARKED = 1,
};

class WebsocketHandler
{
public:
    WebsocketHandler(int _socket);
	~WebsocketHandler();

	int process(int fd,int nread);
	inline char *getbuff();

	void disconnect();

public:
	int   m_socket_;

private:
	char buff_[BUFFLEN];

	WEBSOCKET_STATUS status_;
	WebsocketRequest *request_;
	WebsocketRespond *respond_;

};

inline char *WebsocketHandler::getbuff(){
	memset(buff_, 0, BUFFLEN);
	return buff_;
}


#endif
