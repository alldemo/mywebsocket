//
//  WebsocketRequest.h
//  websocketserver
//
//  Created by cl.zhan on 17/5/25.
//  email:zhanchonglongba@126.com/690759587
//
//

#ifndef WEBSOCKETREQUEST_H_H_
#define  WEBSOCKETREQUEST_H_H_

#include <stdint.h>

class WebsocketRequest
{
public:
	WebsocketRequest();
	~WebsocketRequest();

	bool SetSocketFd(int fd);
	int process(int fd, char *buf, unsigned int maxsize, bool b_conn);

private:

private:

};


#endif